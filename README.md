# Tools to examine data

## Run pyspark with jupyter
`IPYTHON_OPTS="notebook --ip='*' --no-browser" pyspark`

## Install pySpark on OSX
1. Download Java JDK
2. Download the spark archive and unzip it
3. Add `spark_folder/bin` to `PATH`
4. `PYSPARK_DRIVER_PYTHON=ipython PYSPARK_DRIVER_PYTHON_OPTS="notebook" pyspark`

## PySpark guide
1. [Programming guide](https://spark.apache.org/docs/1.6.0/sql-programming-guide.html)
2. [DataFrame methods](https://spark.apache.org/docs/1.6.0/api/python/pyspark.sql.html#pyspark.sql.DataFrame)
3. [Row methods](https://spark.apache.org/docs/1.6.0/api/python/pyspark.sql.html#pyspark.sql.Row)
4. [PySpark SQL function](https://spark.apache.org/docs/1.6.0/api/python/pyspark.sql.html#module-pyspark.sql.functions)


## Useful HDFS commands
+ Copy from local to HDFS: `hdfs dfs -copyFromLocal localpath` or `hdfs dfs -put localpath`
+ Copy from HDFS to local: `hdfs dfs -get hdfspath localpath`
+ [List of other commads](https://hadoop.apache.org/docs/current/hadoop-project-dist/hadoop-common/FileSystemShell.html)
