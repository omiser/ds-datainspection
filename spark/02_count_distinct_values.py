#!/usr/bin/env python
# -*- coding: UTF-8 -*-

""" Read and manipulate CSV file """

from pyspark import SparkConf, SparkContext, SQLContext

# pyspark_csv.py is an ajacent file
# requires "pip install -U dateutils"
import pyspark_csv as pycsv

def main():
    """ entry point """
    sconf = SparkConf()\
        .setAppName("02_count_distinct_values")\
        .setMaster("yarn-client")
    sc = SparkContext(conf=sconf)
    sc.setLogLevel("WARN")
    sqlc = SQLContext(sc)

    # distribute pyspark_csv.py to all executors
    sc.addPyFile('pyspark_csv.py')

    # read data from HDFS file
    # for local files you could use file:/// but this file should be reachable at every executor's host
    lines = sc.textFile("hdfs:///user/timofei/train.csv")

    # some additional options https://github.com/seahboonsiew/pyspark-csv
    df = pycsv.csvToDataFrame(sqlc, lines)

    # df is an object of DataFrame type (Not an RDD)
    # https://spark.apache.org/docs/1.6.0/sql-programming-guide.html#tab_python_2

    print("\n-------------------\n")

    print("\nDistribution by Sex:\n")
    df.groupBy("Sex").count().show()

    print("\nDistibution by Sex among those whose Age is over 38:\n")
    df.filter(df['Age']>38).groupBy("Sex").count().show()

    print("\nhistogram of Age in 10 bins (list_of_bin_boundaries, list_of_bin_counts):\n")
    print(df.select(df.Age).rdd.map(lambda x: x[0]).histogram(10))

    print("\n-------------------\n")

if __name__ == "__main__":
    main()
