#!/usr/bin/env python
# -*- coding: UTF-8 -*-

""" Simple script for printing hostnames of hosts where spark executors are running """

# to run this script use:
#   spark-submit 01_test_parallelization.py

import os
from pyspark import SparkConf, SparkContext

def hostnameMapper(it_part):
    """ returns hostname  """
    from socket import gethostname

    total = sum([1 for i in it_part])
    #return tuple, not string. String will be splitted in symbols
    return ([gethostname(), total], )


def main():
    """ entry point """
    # create spark context
    sconf = SparkConf()\
        .setAppName("01_test_parallelization")\
        .setMaster("yarn-client")
    sc = SparkContext(conf=sconf)
    sc.setLogLevel("WARN")

    print("\n------------------------------------\n")

    rdd = sc.parallelize(range(0, 1000000), 20)
    print("rdd.getNumPartitions() = ", rdd.getNumPartitions())

    rdd_hosts = rdd.mapPartitions(hostnameMapper)

    # materialize and print RDD
    print(rdd_hosts.collect())
    print("\n------------------------------------\n")

if __name__ == "__main__":
    main()
